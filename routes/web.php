<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Verifica se o usuário está logado
Route::get('admin/login','HomeController@login');
Route::post('admin/login','HomeController@postLogin');     
Route::get('admin/logout','HomeController@logout');

Route::group(['middleware' => ['web'] ], function(){  
    #home        
    Route::get('admin','Dashboard@index');
    Route::get('admin/dashboard','Dashboard@index');
    #tarefas
    Route::get('admin/task','Task@index');    
    Route::get('admin/task/form','Task@form');
    Route::get('admin/task/form/{idtask}','Task@form');
    Route::get('admin/task/{idtask}','Task@delete');
    Route::post('admin/task','Task@save');
    #usuarios
    Route::get('admin/user','User@index');    
    Route::get('admin/user/form','User@form');
    Route::get('admin/user/form/{iduser}','User@form');
    Route::get('admin/user/{iduser}','User@delete');
    Route::post('admin/user','User@save');

});



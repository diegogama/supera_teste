<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('idtask');
            $table->integer('iduser');              
            $table->date('datestart_task');           
            $table->date('dateend_task');
            $table->string('title_task',200);
            $table->text('description_task');
            $table->integer('progress_task');    
            $table->integer('priority_task');         
            $table->integer('status_task');  
            $table->timestamps();          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tasks');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Datetime;
use Session;

class Tasks extends Model
{
    protected $table = 'tasks';    
    protected $primaryKey = 'idtask';
    //insert task
    public static function saveTask($dados)
    {        
        $dateStart = new Datetime(str_replace('/','-',$dados['datestart_task']));       
        $dateEnd = new Datetime(str_replace('/','-',$dados['dateend_task']));
        if( $dados['idtask'] ){
            $task = Tasks::find($dados['idtask']);  
            Session::flash('message', 'Tarefa editada com sucesso!');           
        }else{
            $task = new Tasks();            
            Session::flash('message', 'Tarefa cadastrada com sucesso!'); 
        }
        $task->iduser           = $dados['iduser'];
        $task->title_task       = $dados['title_task'];
        $task->datestart_task   = $dateStart->format('Y-m-d');
        $task->dateend_task     = $dateEnd->format('Y-m-d');
        $task->priority_task    = $dados['priority_task'];
        $task->progress_task    = $dados['progress_task'];
        $task->description_task = $dados['description_task'];
        $task->status_task      = 1;
        $task->save();        
    }

    //list task
    public static function listTasks()
    {
        $tasks = Tasks::join('users', 'tasks.iduser', '=', 'users.iduser')
                      ->selectRaw("tasks.*,users.name,DATE_FORMAT(datestart_task,'%d/%m/%Y') AS dts,DATE_FORMAT(dateend_task,'%d/%m/%Y') AS dte")
                      ->get();                 
        return $tasks;
    }

    public static function indicatorsTasks()
    {
        $tasks = Tasks::selectRaw('COUNT(tasks.idtask) AS total_task,tasks.progress_task') 
                        ->groupBy('progress_task')
                        ->orderBy('progress_task')
                        ->get(); 
                          
        $indicators = [
            'backlog'     => $tasks[0]->total_task,
            'doing'       => $tasks[1]->total_task,
            'done'        => $tasks[2]->total_task,
            'total_tasks' => ($tasks[0]->total_task+$tasks[1]->total_task+$tasks[2]->total_task)
        ];
        return $indicators;
    }

    public static function deleteTask($idTask=NULL)
    {
        $task = Tasks::find($idTask);        
        if( $idTask != NULL ){
           Session::flash('message', 'Tarefa excluída com sucesso!'); 
           $task->delete();
        }   
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ValidateTask extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {                
        $rules = [
             'iduser'           => 'required',
             'title_task'       => 'required',
             'datestart_task'   => "required",
             'dateend_task'     => "required",
             'priority_task'    => "required",
             'progress_task'    => "required",
             'description_task' => "required"                            
        ];        
        return $rules;
    }

    public function messages()
    {        
        $messages = [
             'iduser.required'           => "Usuário é obrigatório!",
             'title_task.required'       => "Título é obrigatório!",
             'datestart_task.required'   => "Data início é obrigatório!",
             'dateend_task.required'     => "Data final é obrigatório!",
             'priority_task.required'    => "Prioridade é obrigatório!",
             'progress_task.required'    => "Progresso é obrigatório!",
             'description_task.required' => "Descrição é obrigatório!"                   
        ];        
        return [];            
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ValidateUser extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        $uniqueMail = ( Request::input('iduser') == "" )?'|unique:users':'';
        $rules = [
             'name'  => 'required',
             'email' => "required$uniqueMail"                            
        ];
        if( Request::input('password') != "" || Request::input('iduser') == ""  ){
            $rules['password'] = 'min:6|required';
            $rules['password_confirmed'] = 'min:6|same:password';
        }
        return $rules;
    }

    public function messages()
    {        
        $messages = [
             'name.required' => 'Nome é obrigatório',    
             'email.required' => 'E-mail é obrigatório'                  
        ];
        if( Request::input('iduser') == "" ){
            $messages['email.unique'] = 'E-mail já existe!';
        }
        if( Request::input('password') != "" || Request::input('iduser') == "" ){
            $messages['password.min']           = 'Mínimo de 6 caracteres para senha';
            $messages['password.required']      = 'Senha é obrigatório';
            $messages['password_confirmed.min'] = 'Mínimo de 6 caracteres para senha de confirmação';
            $messages['password_confirmed.same'] = 'As senhas não são iguais!';
        }
        return [];#$messages;            
    }
}

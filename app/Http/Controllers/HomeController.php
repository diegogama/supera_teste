<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        #$this->middleware('auth:web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email'    => 'required|min:20|max:200',
            'password' => 'required|min:8|max:200',
        ];
        $messages = [
            'email.required'    => 'O campo de email é obrigatório',
            'email.min'         => '20 caracteres no mínimo',
            'email.max'         => '200 caracteres no máximo',
            'password.required' => 'O campo de senha é obrigatório',
            'password.min'      => '8 caracteres no mínimo',
            'password.max'      => '200 caracteres no máximo'
        ];

        $validator = validator($request->all(),$rules,$messages);

        if( $validator->fails() ){
            return redirect('admin/login')
                ->withErrors($validator)
                ->withInput();    
        }
        $credentials = [
            'email'    => $request->get('email'),
            'password' => $request->get('password'),
            'status'   => 1
        ];
        if( auth()->guard('web')->attempt($credentials) ){
            return redirect('admin/dashboard');
        }else{
            return redirect('admin/login')
                ->withErrors(['errors' => 'Dados inválidos!'])
                ->withInput();    
        }

    }

    public function logout(){
        auth()->guard('web')->logout();
        return redirect('admin/login');
    }


}

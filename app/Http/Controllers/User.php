<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateUser;
use App\Users;

class User extends Controller
{
    
    public static $statusUser = ['Inativo','Ativo'];
    public static $labelStatusUser = ['label-danger','label-success'];

    public static function index(){
        $users = Users::listUsers();
        return view(
            'user.index',
            array(
                'users'           => $users,
                'statusUser'      => self::$statusUser,
                'labelStatusUser' => self::$labelStatusUser
            )
        );
    }


    public static function form($idUser=null){
        $user = Users::find($idUser);                   
        return view(
            'user.form',
            array(
                'user'       => $user,
                'statusUser' => self::$statusUser                
            )
        );        
    }

    public static function delete($idUser=NULL){
        $user = Users::deleteUser($idUser);
        return redirect('admin/user');     
    }

    public static function save(ValidateUser $request){               
        Users::saveUser($request->all());
        return redirect('admin/user');
    }    

}

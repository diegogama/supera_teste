<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateTask;
use App\Tasks;
use App\Users;

class Task extends Controller
{

   public static $listPriority   = ['High','Urgent','Low','Average'];
   public static $listProgress   = ['Backlog','Doing','Done'];
   public static $statusProgress = ['label-primary','label-warning','label-success','label-danger'];
   
   public function __construct()
   {
        #$this->middleware('auth:web');
   }

   public static function index(){
        $tasks = Tasks::listTasks();
        return view(
            'task.index',
            array(
                'tasks'          => $tasks,
                'progress'       => self::$listProgress,
                'statusProgress' => self::$statusProgress
            )
        );
    }

    public static function form($idTask=null){
        $task = Tasks::listTasks()
                     ->find($idTask);                   
        return view(
            'task.form',
            array(
                'task'       => $task,
                'users'      => Users::listUsers(),
                'priorities' => self::$listPriority,
                'progress'   => self::$listProgress
            )
        );        
    }

    public static function delete($idTask=NULL){
        $task = Tasks::deleteTask($idTask);
        return redirect('admin/task');     
    }

    public static function save(ValidateTask $request){               
        Tasks::saveTask($request->all());
        return redirect('admin/task');
    }   

}

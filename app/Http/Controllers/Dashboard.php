<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboards;
use App\Tasks;


class Dashboard extends Controller
{
    //
    public function __construct()
    {
        #$this->middleware('auth:web');
    }

    public static function index()
    {
        $indicators = Tasks::indicatorsTasks();
        
        return view(
            'dashboard.index',
            array(
                'indicators' => $indicators    
            )
        );
    }
}

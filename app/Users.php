<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Users extends Model
{
    protected $table = 'users';    
    protected $primaryKey = 'iduser';
    //insert user
    public static function saveUser($dados)
    {        
               
        if( $dados['iduser'] ){
            $user = Users::find($dados['iduser']);   
            Session::flash('message', 'Usuário editado com sucesso!');           
        }else{
            $user = new Users();        
            Session::flash('message', 'Usuário cadastrado com sucesso!');    
        }
        $user->name   = $dados['name'];
        $user->email  = $dados['email'];
        $user->status = $dados['status'];
        if( $dados['password'] ){
            $user->password = bcrypt($dados['password']);
        }        
        $user->save();        
    }

    //list user
    public static function listUsers()
    {
        $users = Users::all();
        return $users;
    }

    public static function deleteUser($idUser=NULL)
    {

        $user = Users::find($idUser);        
        if( $idUser != NULL ){
           Session::flash('message', 'Usuário excluído com sucesso!');  
           $user->delete();
        }
    }
    
}

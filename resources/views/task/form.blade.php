<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Task | Form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('menu_top')
  @include('menu_left')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tasks        
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="/admin/task">Tasks</a></li>
        <li class="active">Form Task</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- quick email widget -->
          <div class="box box-info">            
            <div class="box-body">
              @if( count($errors) > 0 )
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert Form Task!</h4>
                <ul>
                  @foreach( $errors->all() as $error )
                  <li>{{$error}}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              <form action="{{ action('Task@save') }}" method="post">
                <div class="form-group col-xs-8">
                  <label>Title</label>
                  <input type="text" class="form-control" name="title_task" value="{{old('title_task',$task->title_task??NULL)}}">
                </div>
                <div class="form-group col-xs-4">
                  <label>Users</label>
                  <select class="form-control" name="iduser">
                    <option selected value="">Select user</option>
                    @foreach( $users as $user )
                    <option {{ ( old('iduser',$task->iduser??NULL) == $user->iduser )?'selected':'' }} value="{{$user->iduser}}">{{$user->name}}</option>
                    @endforeach
                  </select>
                </div>                               
                <div class="form-group col-xs-3">
                  <label>Date Start</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" readonly="readonly" class="form-control pull-right" id="datestart_task" name="datestart_task" value="{{old('datestart_task',$task->dts??NULL)}}">
                  </div>
                </div>
                <div class="form-group col-xs-3">
                  <label>Date End</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" readonly="readonly" class="form-control pull-right" id="dateend_task" name="dateend_task" value="{{old('dateend_task',$task->dte??NULL)}}">
                  </div>
                </div>
                <div class="form-group col-xs-3">
                  <label>Priority</label>
                  <select class="form-control" name="priority_task">
                    @foreach( $priorities as $index => $value )
                    <option {{ ( old('priority_task',$task->priority_task??NULL) == $index )?'selected':'' }} value="{{$index}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-xs-3">
                  <label>Progress</label>
                  <select class="form-control" name="progress_task">
                    @foreach( $progress as $index => $value )
                    <option {{ ( old('progress_task',$task->progress_task??NULL) == $index )?'selected':'' }} value="{{$index}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-xs-12">
                  <label>Description</label>
                  <textarea class="textarea" id="description_task" name="description_task" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                  {{old('description_task',$task->description_task??NULL)}}  
                  </textarea>                  
                </div>                                                
                <div class="box-footer">
                  <input type="hidden" class="form-control" name="idtask" value="{{old('idtask',$task->idtask??NULL)}}">
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                  <button type="submit" class="pull-right btn btn-default">Save
                    <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </form>
            </div>
          </div>          
        </div>
        <!--/.col (left) -->        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('footer')  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script type="text/javascript">
//Date picker
$('#datestart_task').datepicker({
  autoclose: true,
  format:'dd/mm/yyyy'
});
$('#dateend_task').datepicker({
  autoclose: true,
  format:'dd/mm/yyyy'
});
</script>
</body>
</html>
